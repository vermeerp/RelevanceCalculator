package com.pikiro;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;


/**
 * The RelevanceCalculator determines the number of matches of an input word or phrase 
 * (single token) against a set of files, and ranks the files based on number of exact matches.  
 * The input files are parsed based on the assumption that terms are separated by
 * white space.
 *
 * The user is able to enter anything other than a blank string as the search term.
 * 
 * The regular expression search may return different results than the string match search 
 * or index search, since a user could enter a regular expression that matches
 * differently from the exact character match of the other searchers.
 * 
 * The search is case-sensitive.
 * 
 *  The command line input to the program is the directory containing the set of files
 *  to rank.  Only "regular files" as defined by Files::isRegularFile in the directory are 
 *  included in the analysis.
 * 
 * 
 * @author Pamela Vermeer
 *
 */
public class RelevanceCalculator {

    enum SearchMethod {
        MATCH, REGEX, INDEX, LUCENE
    };
    private SearchMethod searchMethod;
    private String searchTerm;
    private String fileDirectoryName;
    private final ConsoleIO consoleIO;
    
    public static void main(String[] args) {  
         
        RelevanceCalculator calculator = new RelevanceCalculator();  
        
        if (!calculator.isValidFileDirectory(args)) {
            System.exit(1);
        };
        
        calculator.initializeSearchParams();
        calculator.execute();
    }   

    public RelevanceCalculator() {
        consoleIO = new ConsoleIO();
    }

    /*
     * Get the search term and method
     */
    private void initializeSearchParams() {
        searchTerm = consoleIO.initializeSearchTerm();
        searchMethod = consoleIO.initializeSearchMethod();
    }

    /*
     * Execute the search
     */
    private void execute() {
        SearchDriver searchDriver = new SearchDriver(searchTerm, searchMethod, fileDirectoryName);
        Collection<SearchResult> results = searchDriver.execute();
        consoleIO.outputResults(results);
    }

    /*
     * Ensure the command line argument is a single argument with a valid directory name.
     */
    boolean isValidFileDirectory(final String[] args) {
        
        // Must be exactly 1 argument
        if (args == null || args.length != 1) {
            System.err.println("Usage: java RelevanceCalculator <Path to directory of files>");
            return false;
        }
        
        // Must be readable, and must be a directory
        fileDirectoryName = args[0];
        final Path fileDirectory = Paths.get(fileDirectoryName);
        if (!Files.isReadable(fileDirectory)) {
            System.err.println("'" + fileDirectory.toAbsolutePath() + "' does not exist or is not readable, please check the path");
            return false;
        }
        if (!Files.isDirectory(fileDirectory)) {
            System.err.println("D'" + fileDirectory.toAbsolutePath() + "' is not a directory.");
            return false;        
        }
        return true;
    }
}
