package com.pikiro;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import com.pikiro.RelevanceCalculator.SearchMethod;

/**
 * Test ConsoleIO
 * 
 * @author Pamela Vermeer
 *
 */
public class ConsoleIOTest {

    ConsoleIO consoleIO;

    @Before
    public void setup() {
        consoleIO = new ConsoleIO();
    }

    @Test
    public void testValidSearchTerm() {

        final String data = "Hello, World!";
        final InputStream stdin = System.in;

        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            final String actual = consoleIO.initializeSearchTerm();
            assertEquals("Search term should be " + data + " but is " + actual, data, actual);
        } finally {
            System.setIn(stdin);
        }
    }

    @Test
    public void testEmptySearchTerm() {

        final String data = " ";
        final InputStream stdin = System.in;
        final OutputStream stdout = System.out;
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();

        String expected = "Enter a search term with at least one non-white space character:";
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            System.setOut(new PrintStream(myOut));

            consoleIO.initializeSearchTerm();
            final String actual = myOut.toString();
            assertEquals("Expected user prompt", expected, actual);
        } finally {
            System.setIn(stdin);
            System.setOut(new PrintStream(stdout));
        }
    }

    @Test
    public void testValidSearchMethod() {
        
        String data = "1";
        final InputStream stdin = System.in;
        
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            SearchMethod actual = consoleIO.initializeSearchMethod();
            assertEquals("Search method should be " + SearchMethod.MATCH + " but is " + actual, SearchMethod.MATCH, actual);

            data = "2";
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            actual = consoleIO.initializeSearchMethod();
            assertEquals("Search method should be " + SearchMethod.REGEX + " but is " + actual, SearchMethod.REGEX, actual);

            data = "3";
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            actual = consoleIO.initializeSearchMethod();
            assertEquals("Search method should be " + SearchMethod.INDEX + " but is " + actual, SearchMethod.INDEX, actual);

        } finally {
            System.setIn(stdin);
        }
    }

    @Test
    public void testInvalidSearchMethod() {

        String data = "51 3";
        final InputStream stdin = System.in;
        final OutputStream stdout = System.out;
        ByteArrayOutputStream myOut = new ByteArrayOutputStream();

        // Output stream should have two copies of the prompt, one for invalid
        // entry, one for valid entry
        final String prompt = "Choose your preferred search method:\n\t1) String Match\n\t2) Regular Expression\n\t3) Indexed\n\t4) Lucene Index\nEnter 1, 2, 3, or 4: ";
        final String expected = prompt + prompt;

        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            System.setOut(new PrintStream(myOut));
            consoleIO.initializeSearchMethod();
            String actual = myOut.toString();
            assertEquals("Expected two copies of user prompt", expected, actual);

            data = "PJV 2";
            myOut = new ByteArrayOutputStream();
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            System.setOut(new PrintStream(myOut));
            consoleIO.initializeSearchMethod();
            actual = myOut.toString();
            assertEquals("Expected two copies of user prompt", expected, actual);

        } finally {
            System.setIn(stdin);
            System.setOut(new PrintStream(stdout));
        }
    }
    
    @Test
    public void testOutputResults() {
        
        final Collection<SearchResult> results = new TreeSet<SearchResult>();
        SearchResult result = new SearchResult(52, 12, "file_1");
        results.add(result);
        result = new SearchResult(22, 8, "file_2");
        results.add(result);
        result = new SearchResult(502, 17, "file_3");
        results.add(result);
        
        final OutputStream stdout = System.out;
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();

        final String expected = "file_3: 502 matches\n" + 
                "file_1: 52 matches\n" + 
                "file_2: 22 matches\n" + 
                "Elapsed Time: 37ms\n";
        try {
            System.setOut(new PrintStream(myOut));
            consoleIO.outputResults(results);            
            final String actual = myOut.toString();
            assertEquals(expected, actual);
        } finally {
            System.setOut(new PrintStream(stdout));
        }
    }
}
