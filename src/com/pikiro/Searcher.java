package com.pikiro;

/**
 * Base class for searching algorithms.
 * 
 * @author Pamela Vermeer
 *
 */
public abstract class Searcher {
            
    /*
     *  Execute the search for a term, tracking the time it takes to do the search, 
     *  and compiling and returning the results.
     */
    public SearchResult executeSearch(final String fileToSearch, final String searchTerm) {
        final SearchResult result = new SearchResult();
                
        final long startTime = System.currentTimeMillis();
        final int termCount = countOccurrences(fileToSearch, searchTerm);
        final long endTime = System.currentTimeMillis();
        result.setCount(termCount);
        result.setSearchTime(endTime - startTime);
        result.setFileName(fileToSearch);
        
        return result;
    }
    
    /*
     * Each search algorithm must implement this to count the number of times
     * the search term occurs in the file.
     */
    abstract int countOccurrences(String fileName, String searchTerm);
    
    /*
     * Verify that the character to the left of the found term is a delimiter or first character in the line.
     */
    protected boolean isDelimiterToTheRight(final String line, final int end) {
        if (end == line.length()) {
            return true;
        }
        
        // Safe to use end + 1 because we know we aren't past the EOL
        
        if (Character.isWhitespace(line.charAt(end))) {
            return true;
        }
        return false;
    }

    /*
     * Verify that the character to the left of the found term is a delimiter or first character in the line.
     */
    protected boolean isDelimiterToTheLeft(final String line, final int start) {
        if (start == 0) {
            return true;
        }

        // Safe to use start - 1 because we know we aren't past the EOL
        if (Character.isWhitespace(line.charAt(start-1))) {
            return true;
        }
        return false;
    }
}
