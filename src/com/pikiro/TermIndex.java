package com.pikiro;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Holds a map from each term in the files in the directory to a map from each filename to the count 
 * of the number of times the term is in the file.
 *
 * @author Pamela Vermeer
 *
 */
public class TermIndex {
    
    Map<String, HashMap<String, Integer>> theIndex;
    private String documentDirectoryName;
    
    public TermIndex(final String documentDirectoryName) {
        this.documentDirectoryName = documentDirectoryName;
        theIndex = new HashMap<String, HashMap<String, Integer>>();
        createIndex();
    }

    /*
     * For each file in the document directory, add its terms to the index.
     */
    private void createIndex() {

        try (Stream<Path> paths = Files.walk(Paths.get(documentDirectoryName))) {
            paths.filter(Files::isRegularFile).forEach(path -> indexFile(path.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }

    /*
     * Index the terms in a single file.
     */
    private void indexFile(final String fileName) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String line;
            while ((line = br.readLine()) != null) {
                String[] tokens = line.split("\\s");
                
                for (int i = 0; i < tokens.length; i++) {
                    addTokenToIndex(fileName, tokens[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (Exception e) {
                System.err.println("Exception while closing bufferedreader " + e.toString());
            }
        }
    }

    /*
     * Index a single term within a file.
     */
    private void addTokenToIndex(final String fileName, final String term) {

        // If this term has not been entered in the index yet, create the map to store it
        HashMap<String, Integer> termMap = theIndex.get(term);
        if (termMap == null) {
            termMap = new HashMap<String, Integer>();
            theIndex.put(term, termMap);
        }
        
        // If this term has not been entered for this file, create an entry for this file
        Integer termCountForFile = termMap.get(fileName);
        if (termCountForFile == null) {
            termCountForFile = 0;
            termMap.put(fileName, termCountForFile);
        }
        
        // Replace value in the index with incremented value
        termCountForFile++;
        termMap.put(fileName, termCountForFile);
    }

    /*
     * Lookup and return the count of times a term is found in a file.
     */
    public int getCount(final String fileName, final String searchTerm) {

        if (theIndex == null || theIndex.isEmpty()) {
            return 0;
        }
        
        Map<String, Integer> termMap = theIndex.get(searchTerm);
        if (termMap == null || termMap.isEmpty()) {
            return 0;
        }
        
        Integer termCount = termMap.get(fileName);
        if (termCount == null) {
            return 0;
        }
        
        return termCount;
    }
}
