package com.pikiro;

import java.util.List;

/**
 * Lucene search results consists of the list of files in descending
 * relevance order.
 * 
 * @author Pamela Vermeer
 *
 */
public class LuceneSearchResult extends SearchResult {

    private List<String> documentOrder;

    public List<String> getDocumentOrder() {
        return documentOrder;
    }

    public void setDocumentOrder(final List<String> documentOrder) {
        this.documentOrder = documentOrder;
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("The relevance order of the files is:\n");
        documentOrder.forEach(document -> builder.append(document).append("\n"));
        return builder.toString();
    }
}
