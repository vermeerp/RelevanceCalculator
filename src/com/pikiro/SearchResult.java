package com.pikiro;

/**
 * Search results consist of the file, the number of times the term appears in
 * the file, and the time to do the search.  Implements Comparable to display the 
 * results in sorted order.
 * 
 * @author Pamela Vermeer
 *
 */
public class SearchResult implements Comparable<SearchResult>{
    
    private int count;
    private long searchTime;
    private String fileName;
    
    public SearchResult() {
    }

    public SearchResult(final int count, final long searchTime, final String fileName) {
        this.count = count;
        this.searchTime = searchTime;
        this.fileName = fileName;
    }
    
    public String toString() {
        return fileName + ": " + count + " matches";
    }

    /*
     * Sort from high count to low count, alphabetical sorting by filename if count is equal
     */
    @Override
    public int compareTo(final SearchResult other) {
        final int BEFORE = 1;
        final int AFTER = -1;
        // Primary comparison is the count
       if (this.count < other.count) {
            return BEFORE;
        } else if (this.count > other.count) {
            return AFTER;
        } 
       
       // Count equal, use fileName to order
       return this.fileName.compareTo(other.fileName);
 
    }

    public int getCount() {
        return count;
    }

    public void setCount(final int count) {
        this.count = count;
    }

    public long getSearchTime() {
        return searchTime;
    }

    public void setSearchTime(final long searchTime) {
        this.searchTime = searchTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }
}
