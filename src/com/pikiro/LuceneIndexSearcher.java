package com.pikiro;

import java.io.BufferedReader;

/**
 * File indexing and searching code adapted from Lucene 7.3.1 demo code, licensed under the
 * Apache License, Version 2.0.
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Uses Lucene to create an index, search, and  provide feedback about relevance order of the files. 
 */


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class LuceneIndexSearcher extends Searcher {
    
    private String indexPathName;
    private long indexingTime;
    private List<String> documentOrder;
    private int badPatternCounter = 0;


    /*
     * Create the index in a folder whose name is the the input fileDirectoryName with _index
     * appended. The index is created or indexed if createNewIndex is false. 
     */
    public LuceneIndexSearcher(final String fileDirectoryName, final boolean createNewIndex) {
        // TODO 
        // * Check if fileDirectory is a valid file
        // * Check if fileDirectoryName_index is available
        indexPathName = fileDirectoryName + "_index";
        
        final Path documentDirectory = Paths.get(fileDirectoryName);
        final Date start = new Date();
        try {
            final Directory indexDirectory = FSDirectory.open(Paths.get(indexPathName));
            final Analyzer analyzer = new StandardAnalyzer();
            final IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (createNewIndex) {
                // Create a new index in the directory, removing any
                // previously indexed documents:
                iwc.setOpenMode(OpenMode.CREATE);
            } else {
                // Add new documents to an existing index:
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }
            
            final IndexWriter writer = new IndexWriter(indexDirectory, iwc);
            indexDocs(writer, documentDirectory);
            
            writer.close();
            final Date end = new Date();
            indexingTime = end.getTime() - start.getTime();
        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
        }
    }
    
    /*
     * Indexes the files in documentDirectory
     */
    private void indexDocs(final IndexWriter writer, final Path documentDirectory) throws IOException {
        if (Files.isDirectory(documentDirectory)) {
            Files.walkFileTree(documentDirectory, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    try {
                        indexDoc(writer, file, attrs.lastModifiedTime().toMillis());
                    } catch (IOException ignore) {
                        // don't index files that can't be read.
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } else {
            indexDoc(writer, documentDirectory, Files.getLastModifiedTime(documentDirectory).toMillis());
        }
    }

    /*
     * Index a single document.  
     */
    void indexDoc(final IndexWriter writer, final Path file, final long lastModified) throws IOException {
        try (InputStream stream = Files.newInputStream(file)) {
            // make a new, empty document
            final Document doc = new Document();

            // Add the path of the file as a field named "path". Use a
            // field that is indexed (i.e. searchable), but don't tokenize
            // the field into separate words and don't index term frequency
            // or positional information:
            Field pathField = new StringField("path", file.toString(), Field.Store.YES);
            doc.add(pathField);

            // Add the last modified date of the file a field named "modified".
            // Use a LongPoint that is indexed (i.e. efficiently filterable with
            // PointRangeQuery). This indexes to milli-second resolution, which
            // is often too fine. You could instead create a number based on
            // year/month/day/hour/minutes/seconds, down the resolution you
            // require.
            // For example the long value 2011021714 would mean
            // February 17, 2011, 2-3 PM.
            doc.add(new LongPoint("modified", lastModified));

            // Add the contents of the file to a field named "contents". Specify
            // a Reader,
            // so that the text of the file is tokenized and indexed, but not
            // stored.
            // Note that FileReader expects the file to be in UTF-8 encoding.
            // If that's not the case searching for special characters will
            // fail.
            doc.add(new TextField("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));

            if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
                // New index, so we just add the document (no old document can
                // be there):
                writer.addDocument(doc);
            } else {
                // Existing index (an old copy of this document may have been
                // indexed) so
                // we use updateDocument instead to replace the old one matching
                // the exact
                // path, if present:
                writer.updateDocument(new Term("path", file.toString()), doc);
            }
        }
    }
    
    /*
     *  Execute the search for a term, tracking the time it takes to do the search, 
     *  and compiling and returning the results. 
     *  
     *  This is a kludgy way to create the result to fit into my current architecture.
     *  By setting the count and filename to the same thing, only the first result will 
     *  be put into the TreeSet used for the results.  
     */
    public SearchResult executeSearch(final String fileToSearch, final String searchTerm) {
        final LuceneSearchResult result = new LuceneSearchResult();
                
        final long startTime = System.currentTimeMillis();
        countOccurrences(fileToSearch, searchTerm);
        final long endTime = System.currentTimeMillis();
        
        result.setCount(0);
        result.setSearchTime(endTime - startTime);
        result.setFileName("Lucene Index Search");
        result.setDocumentOrder(documentOrder);
        
        return result;
    }
    
    @Override
    int countOccurrences(String fileName, String searchTerm) {
        documentOrder = new ArrayList<String>();
        try {
            IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(indexPathName)));
            org.apache.lucene.search.IndexSearcher searcher = new org.apache.lucene.search.IndexSearcher(reader);
            Analyzer analyzer = new StandardAnalyzer();

            QueryParser parser = new QueryParser("contents", analyzer);
            Query query = parser.parse(searchTerm);
            TopDocs topDocs = searcher.search(query, 10);

            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document doc = searcher.doc(scoreDoc.doc);
                documentOrder.add(doc.get("path"));
            }

        } catch (ParseException | IOException e) {
            badPatternCounter++;
        }
        return 0;
    }

    public long getIndexingTime() {
        return indexingTime;
    }

    public List<String> getDocumentOrder() {
        return documentOrder;
    }

    public int getBadPatternCount() {
        return badPatternCounter;
    }

}
