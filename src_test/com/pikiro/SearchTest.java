package com.pikiro;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

/**
 * IndexSearcher and MatchSearcher return the same results, so they can
 * share the same tests.
 * 
 * @author Pamela Vermeer
 *
 */
public class SearchTest {
    final String TEST_FILE_DIRECTORY = "test_files";
    final String TEST_FILE_ZERO = "zero_line_file.txt";
    final String TEST_FILE_ONE = "one_line_file.txt";
    final String TEST_FILE_TWO = "two_empty_lines.txt";
    final String TEST_FILE_THREE = "text_with_numbers.txt";

    Searcher searcher;
    
    @Test
    public void testCountingMatchAtStartOfTheLine() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_ONE, "she");
        assertEquals("she appears in this file 2 times", 2, result.getCount());
    }
    
    @Test
    public void testCountingMatchAtEndOfTheLine() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_ONE, "She");
        assertEquals("She appears in this file 3 times", 3, result.getCount());
    }

    @Test
    public void testNoMatches() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_ONE, "XXX");
        assertEquals("XXX appears in this file 0 times", 0, result.getCount());
    }
  
    @Test
    public void testEmptyFile() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_ZERO, "XXX");
        assertEquals("XXX appears in this file 0 times", 0, result.getCount());
    }
    
    @Test
    public void testEmptyLineFile() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_TWO, "XXX");
        assertEquals("XXX appears in this file 0 times", 0, result.getCount());
    }
    
    @Test
    public void testSearchForANumber() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_THREE, "10");
        assertEquals("10 appears in this file 3 times", 3, result.getCount());
    }
    
    @Test
    public void testSearchWithBlankSpace() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_THREE, "She w");
        assertEquals("'She w' is invalid search term", 0, result.getCount());
    }
    
    @Test
    public void testSearchWithBlankSpaceAtEnd() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_THREE, "She ");
        assertEquals("'She ' is invalid search term", 0, result.getCount());
    }
    
}
