package com.pikiro;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Run all tests for classes with zero argument constructors.
 * 
 * @author Pamela Vermeer
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ ConsoleIOTest.class, SearchDriverTest.class, MatchSearcherTest.class, RegexSearcherTest.class, RelevanceCalculatorTest.class })
public class AllTests {

}
