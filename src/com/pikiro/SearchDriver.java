package com.pikiro;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Stream;

import com.pikiro.RelevanceCalculator.SearchMethod;

/**
 * Walks through the set of files and gets the count of occurrences of the term using the 
 * selected algorithm for searching.
 * 
 * @author Pamela Vermeer
 *
 */
public class SearchDriver {

    private String searchTerm;
    private SearchMethod searchMethod;
    private String fileDirectory;

    public SearchDriver(final String searchTerm, final SearchMethod searchMethod, final String fileDirectory) {
        super();
        this.searchTerm = searchTerm;
        this.searchMethod = searchMethod;
        this.fileDirectory = fileDirectory;
    }

    /*
     * Execute the search.
     */
    public Collection<SearchResult> execute() {
        final Searcher searcher = getSearcher();
        Collection<SearchResult> results = new TreeSet<SearchResult>();

        try (Stream<Path> paths = Files.walk(Paths.get(fileDirectory))) {
            paths.filter(Files::isRegularFile).forEach(path -> results.add(searcher.executeSearch(path.toString(), searchTerm)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }

    /*
     * Create the desired searcher based on the search method
     */
    Searcher getSearcher() {
        Searcher searcher = null;
        switch (searchMethod) {
        case MATCH:
            searcher = new MatchSearcher();
            break;
        case REGEX:
            searcher = new RegexSearcher();
            break;
        case INDEX:
            searcher = new IndexSearcher(fileDirectory);
            break;
        case LUCENE:
            searcher = new LuceneIndexSearcher(fileDirectory, true);
            break;
        default:
            break;
        }
        return searcher;
    }

}
