package com.pikiro;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Count the number of occurrences of a term in a file using string comparison.
 *
 * @author Pamela Vermeer
 *
 */
public class MatchSearcher extends Searcher {

    @Override
    int countOccurrences(String fileName, String searchTerm) {
        int termCount = 0;
        final int termLength = searchTerm.length();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String line;
            while ((line = br.readLine()) != null) {
                int lineLength = line.length();
                int start = line.indexOf(searchTerm);
                do {
                    if (start != -1) {
                        // Only count as a match if it is white-space delimited
                        if (isDelimiterToTheLeft(line, start) && isDelimiterToTheRight(line, start + termLength)) {
                            termCount++;
                        }

                        // If there is enough room left in the line for another term, continue searching
                        start += termLength;
                        if (termLength + start  < lineLength) {
                            start = line.indexOf(searchTerm, start + 1);
                        } else {
                            start = -1; // won't find the term anymore in the line
                        }
                    }     
                } while (start != -1) ; 
            } 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (Exception e) {
                System.err.println("Exception while closing bufferedreader " + e.toString());
            }
        }

        return termCount;
    }
 


}
