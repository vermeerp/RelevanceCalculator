package com.pikiro;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Count the number of occurrences of a term in a file using regular expression searching.
 *
 * @author Pamela Vermeer
 *
 */
public class RegexSearcher extends Searcher {
    private int badPatternCounter = 0;

    @Override
    int countOccurrences(final String fileName, final String searchTerm) {
        BufferedReader br = null;
        Pattern pattern;
        try {
            pattern = Pattern.compile(searchTerm);
        } catch (PatternSyntaxException e) {
            badPatternCounter++;
            return 0;
        }
        
        Matcher matcher;
        int termCount = 0;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String line;
            while ((line = br.readLine()) != null) {
                matcher = pattern.matcher(line);
                while (matcher.find()) {
                    // Only count as a match if it is white-space delimited
                    int start = matcher.start();
                    int end = matcher.end();
                    if (isDelimiterToTheLeft(line, start) && isDelimiterToTheRight(line, end)) {
                        termCount++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (Exception e) {
                System.err.println("Exception while closing bufferedreader " + e.toString());
            }
        }
        return termCount;

    }
    
    public int getBadPatternCount() {
        return badPatternCounter;
    }

}
