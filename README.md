RelevanceCalculator Instructions and Notes
==========================================
## General Notes
I initially thought I would implement the indexed search using the Lucene
search framework.  I tried that out but couldn't get the match count
out of it, so I abandoned it and wrote my own simple indexed search class.
However I was interested in how that search approach would compare with
what I wrote, so at the end, I added that back in.  When you look at
and run the performance test, you will see that I commented out that
search engine because its performance was so poor.  To pursue that route
further, I would need to spend more time understanding the tool better
and how to set up the index and the search process better.  But it was
interesting to try out.

## Build Instructions

* Requires Java 8 or later available on your system
* Clone the repo to a folder on your system.
* Open a terminal window and navigate to the RelevanceCalculator folder.
* Run the following command  
`javac -d bin/ -cp lib/*:bin:src src/com/pikiro/RelevanceCalculator.java`

## Execution Instructions

* Put the files you want to rank into a single folder.
* To run the program, run the command  
`java -cp lib/*:bin com.pikiro.RelevanceCalculator <path to directory containing files to rank>``

## Test Build Instructions
 * Open a terminal window and navigate to the RelevanceCalculator folder.
 * Run the following command  
`javac -d bin/ -cp lib/*:bin -sourcepath src_test/com/pikiro/*.java`

## Test Execution Instructions

* Be sure that the folders test_files and big_files are in your working
directory with the files from the repo
* Run the following command  
`java -cp lib/*:bin org.junit.runner.JUnitCore  com.pikiro.ConsoleIOTest com.pikiro.SearchDriverTest com.pikiro.IndexSearcherTest com.pikiro.MatchSearcherTest com.pikiro.RegexSearcherTest com.pikiro.RelevanceCalculatorTest com.pikiro.LuceneIndexSearcherTest`

## Performance Test Execution Instructions
* Be sure that the file sample_text/french_armed_forces.txt is available
* Run the following command  
`java -cp lib/*:bin com.pikiro.PerformanceTest`

## Performance Test Output

The expectation is that index search will be significantly faster than either
of the other two approaches, and that simple string matching will be faster
than regular expression matching.  This does play out, as seen with this
typical output from running the performance test:  

Match search time: 95512 ms  
Regular expression search time: 129081 ms  
Index search time: 729 ms  

The reason index search is so much faster is because once the index
is created, each search is an O(1) operation.  Simple string matching has
to consider each character in the file each time a search is performed,
so it is O(n), where n is the number of characters in the file.

It is well-known that regular expression matching is slower than basic
comparison search.  [This article ](https://swtch.com/~rsc/regexp/regexp1.html)
provides details on the potentially exponential performance of regular
expression matching in most common programming languages, including
Java.  Even a fast regular expression matching algorithm for a simple
pattern is shown to be on the order of O(mn), where n is the length
of the string being matched, and m is the length of the regular
expression.

## Improving Performance

I would only focus on the index search algorithm. The main changes
I would make would be:
1. Use threads to search multiple files simultaneously
2. Store and re-use the index, so indexing would not need to be done
on each run of the program.
3. Make sure the hardware has a fast drive, for example use SSD instead
of older technology drives
4. Keep the storage local or reduce/eliminate remote drive access as much
as possible.

I'm quite sure that using a search framework like Lucene would also
be valuable, with more research on how to use it effectively.
