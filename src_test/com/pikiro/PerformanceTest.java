package com.pikiro;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

/**
 * Performance test to compare the different searching algorithms. 
 * 
 * Two different types of random words can be generated.  One uses all ASCII characters, the other 
 * upper and lower case letters and numbers only.  By using the fixed set of characters, we know that
 * regular expression matching will be more similar to the other two matching methods, and will not fail 
 * due to an illegal pattern.
 * 
 * The test file used is fairly small to keep overall execution time down.
 * 
 * Results from one execution:
 * Match search time: 94327 ms
 * Regular expression search time: 121544 ms
 * Index search time: 774 ms
 *
 * @author Pamela Vermeer
 *
 */
public class PerformanceTest {
    private static final int TEST_SIZE = 2000000;
    private static final String TEST_FILE_DIRECTORY = "sample_text";
    private static final String TEST_FILE = "sample_text/french_armed_forces.txt";
    private static final int MIN_TERM_SIZE = 1;
    private static final int MAX_TERM_SIZE = 13;
    private static final String CHARS_FOR_RANDOM_TERMS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    
    private enum RandomWordType {
        ASCII,
        FIXED_SET
    }
    private static final RandomWordType RANDOM_WORD_TYPE = RandomWordType.FIXED_SET;

    public void testSearchPerformance() {
        Searcher matchSearcher = new MatchSearcher();
        Searcher regexSearcher = new RegexSearcher();
        Searcher indexSearcher = new IndexSearcher(TEST_FILE_DIRECTORY);
//        Searcher luceneSearcher = new LuceneIndexSearcher(TEST_FILE_DIRECTORY, true);
       
        long matchTotalTime = 0;
        long regexTotalTime = 0;
        long indexTotalTime = 0;
//        long luceneTotalTime = 0;

         String testWord = "";
         for (int i = 0; i < TEST_SIZE; i++ ) {
             
             testWord = getTestWord();
             matchTotalTime += search(TEST_FILE, testWord, matchSearcher);
             regexTotalTime += search(TEST_FILE, testWord, regexSearcher);
             indexTotalTime += search(TEST_FILE, testWord, indexSearcher);
//             luceneTotalTime += search(TEST_FILE, testWord, luceneSearcher);

         }
         
         System.out.println("Match search time: " + matchTotalTime + " ms");
         System.out.println("Regular expression search time: " + regexTotalTime + " ms");
         System.out.println("Index search time: " + indexTotalTime + " ms");
//         System.out.println("Lucene index search time: " + luceneTotalTime + " ms");
//         System.out.println("Bad patterns: " + ((LuceneIndexSearcher) luceneSearcher).getBadPatternCount());

    }

    /*
     * Require that the test word have at least one non-blank character.
     */
    private String getTestWord() {
        String testWord = "";
        
        do {
            switch (RANDOM_WORD_TYPE) {
            case ASCII:
                testWord = RandomStringUtils.randomAscii(MIN_TERM_SIZE, MAX_TERM_SIZE);
                break;
            case FIXED_SET:
                testWord = RandomStringUtils.random(RandomUtils.nextInt(MIN_TERM_SIZE, MAX_TERM_SIZE), CHARS_FOR_RANDOM_TERMS);
                break;
            }           
        } while (testWord.trim().length() == 0);
        
        return testWord;
    }

    /*
     * Run the search.
     */
    private long search(final String fileName, final String testWord, final Searcher searcher) {
        SearchResult result = searcher.executeSearch(fileName, testWord);
        return result.getSearchTime();
    }

    public static void main(String args[]) {
        PerformanceTest performanceTest = new PerformanceTest();
        performanceTest.testSearchPerformance();
    }
}
