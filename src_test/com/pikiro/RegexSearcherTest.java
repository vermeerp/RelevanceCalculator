package com.pikiro;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

/**
 * Test RegexSearcher
 * 
 * @author Pamela Vermeer
 *
 */
public class RegexSearcherTest extends SearchTest {

    @Before
    public void setUp() throws Exception {
        searcher = new RegexSearcher();
    }
    
    @Test
    public void testValidRegularExpression() {
        final SearchResult result = searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_THREE, "r.d.*");
        assertEquals("The regular expression r.d.* has 2 matches in this file", 2, result.getCount());
    }
    
    @Test
    public void testInvalidRegularExpression() {
        searcher.executeSearch( TEST_FILE_DIRECTORY + File.separator + TEST_FILE_THREE, "hyhJMB)EC<3SMT@U~");
        assertEquals("hyhJMB)EC<3SMT@U~) is a bad pattern", 1, ((RegexSearcher)searcher).getBadPatternCount());
    }
    
}
