package com.pikiro;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;

import org.junit.Test;

import com.pikiro.RelevanceCalculator.SearchMethod;

/**
 * Test SearchDriver
 * 
 * @author Pamela Vermeer
 *
 */
public class SearchDriverTest {

    private final String TEST_FILE_DIRECTORY = "test_files";
    private final String TEST_FILE_ONE = "one_line_file.txt";
    private final String TEST_FILE_TWO = "two_empty_lines.txt";
    private final String TEST_FILE_THREE = "text_with_numbers.txt";
    


    @Test
    public void testGettingSearcher() {

        SearchDriver searchDriver = new SearchDriver("foo", SearchMethod.MATCH, TEST_FILE_DIRECTORY);
        Searcher searcher = searchDriver.getSearcher();
        assertTrue("Searcher is of type MatchSearcher", searcher instanceof MatchSearcher);

        searchDriver = new SearchDriver("foo", SearchMethod.REGEX, TEST_FILE_DIRECTORY);
        searcher = searchDriver.getSearcher();
        assertTrue("Searcher is of type RegexSearcher", searcher instanceof RegexSearcher);

        searchDriver = new SearchDriver("foo", SearchMethod.INDEX, TEST_FILE_DIRECTORY);
        searcher = searchDriver.getSearcher();
        assertTrue("Searcher is of type IndexSearcher", searcher instanceof IndexSearcher);
    }

    @Test
    public void testExecutingASearch() {
        final SearchDriver searchDriver = new SearchDriver("he", SearchMethod.REGEX, TEST_FILE_DIRECTORY);
        final Collection<SearchResult> results = searchDriver.execute();
        assertEquals("There are 4 regular files so should have 4 results", 4, results.size());
        
        Iterator<SearchResult> resultsIterator = results.iterator();
        SearchResult result = resultsIterator.next();
        assertEquals(TEST_FILE_DIRECTORY + File.separator + TEST_FILE_ONE + ": 2 matches", result.toString());
        
        result = resultsIterator.next();
        assertEquals(TEST_FILE_DIRECTORY + File.separator + TEST_FILE_THREE + ": 0 matches", result.toString());
        
        result = resultsIterator.next();
        assertEquals(TEST_FILE_DIRECTORY + File.separator + TEST_FILE_TWO + ": 0 matches", result.toString());
    }
    
    @Test
    public void testResultsWithSameCount() {
        SearchDriver searchDriver = new SearchDriver("She", SearchMethod.MATCH, TEST_FILE_DIRECTORY);
        Collection<SearchResult> results = searchDriver.execute();
        assertEquals("There are 4 regular files so should have 4 results", 4, results.size());
        
        Iterator<SearchResult> resultsIterator = results.iterator();
        SearchResult result = resultsIterator.next();
        assertEquals(TEST_FILE_DIRECTORY + File.separator + TEST_FILE_ONE + ": 3 matches", result.toString());
        
        result = resultsIterator.next();
        assertEquals(TEST_FILE_DIRECTORY + File.separator + TEST_FILE_THREE + ": 3 matches", result.toString());
        
        result = resultsIterator.next();
        assertEquals(TEST_FILE_DIRECTORY + File.separator + TEST_FILE_TWO + ": 0 matches", result.toString());
    }
}
