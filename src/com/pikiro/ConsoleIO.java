package com.pikiro;

import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;

import com.pikiro.RelevanceCalculator.SearchMethod;

/**
 * Handles User input/output operations.
 * Input assumptions: 
 *  1. Must have at least one non-blank character
 *
 * @author Pamela Vermeer
 *  
 */
public class ConsoleIO {

    /*
     * Gets search term from user, and ensures the search term has at least one
     * non-blank character.
     */
    public String  initializeSearchTerm() {

        // Cannot close scanner or we will close System.in
        @SuppressWarnings("resource")
        final Scanner scanner = new Scanner(System.in);
        String searchTerm = null;
        
        do {
            System.out.print("Enter a search term with at least one non-white space character:");
            searchTerm = scanner.nextLine();
        } while (searchTerm.length() == 0);

        return searchTerm;
    }

    /*
     * Gets the search method from the user, ensuring that the selection is
     * valid.
     */
    public SearchMethod initializeSearchMethod() {

        // Cannot close scanner or we will close System.in
        @SuppressWarnings("resource")
        final Scanner scanner = new Scanner(System.in);
        SearchMethod searchMethod = null;
        
        int menuChoice = 0;
        do {
            System.out.print("Choose your preferred search method:\n\t1) String Match\n\t2) Regular Expression\n\t3) Indexed\n\t4) Lucene Index\n");
            System.out.print("Enter 1, 2, 3, or 4: ");

            if (scanner.hasNextInt()) {
                menuChoice = scanner.nextInt();
            } else {
                scanner.next();
            }
        } while (menuChoice != 1 && menuChoice != 2 && menuChoice != 3 && menuChoice != 4);

        switch (menuChoice) {
        case 1:
            searchMethod = SearchMethod.MATCH;
            break;
        case 2:
            searchMethod = SearchMethod.REGEX;
            break;
        case 3:
            searchMethod = SearchMethod.INDEX;
            break;
        case 4:
            searchMethod = SearchMethod.LUCENE;
            break;
        }

        return searchMethod;
    }
    
    /*
     * Print out results from the search.  The output will be sorted since the SearchResults
     * are stored in a TreeMap sorted descending by count, and alphabetically by filename.
     */
    public void outputResults(final Collection<SearchResult> results) {
        long totalTime = 0;

        for (Iterator<SearchResult> iterator = results.iterator(); iterator.hasNext();) {
            SearchResult result = iterator.next();
            System.out.println(result);
            totalTime += result.getSearchTime();
        }
        
        System.out.println("Elapsed Time: " + totalTime + "ms");

    }
}
