package com.pikiro;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class LuceneIndexSearcherTest {

    final String TEST_FILE_DIRECTORY = "big_files";
    Searcher searcher;

    @Before
    public void setUp() throws Exception {
        searcher = new LuceneIndexSearcher(TEST_FILE_DIRECTORY, false);
    }
    
    @Test
    public void testLuceneIndexSearch() {
        final SearchResult actual =  searcher.executeSearch("foo", "girl");
        final String expected = 
                "The relevance order of the files is:\n" + 
                "big_files/A_Yankee_Girl_At_Shiloh.txt\n" + 
                "big_files/Just_A_Girl.txt\n" + 
                "big_files/The_Campfire_Girls.txt\n";
        assertEquals(expected, actual.toString());
    }
}
