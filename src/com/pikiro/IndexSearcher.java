package com.pikiro;

/**
 * Count the number of occurrences of a term in a file by pre-processing files to 
 * create an index, and then looking up term counts from the index.
 *
 * @author Pamela Vermeer
 *
 */
public class IndexSearcher extends Searcher {
    
    TermIndex termIndex;

    // Constructor creates the index
    public IndexSearcher(final String documentDirectoryName) {
          termIndex = new TermIndex(documentDirectoryName);
    }
     
    
    @Override
    int countOccurrences(final String fileName, final String searchTerm) {
        return termIndex.getCount(fileName, searchTerm);
    }

}
