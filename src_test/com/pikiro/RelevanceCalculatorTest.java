package com.pikiro;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

/**
 * Test RelevanceCalculator
 * 
 * @author Pamela Vermeer
 *
 */
public class RelevanceCalculatorTest {

    final String TEST_FILE_DIRECTORY = "test_directory";
    final String NOT_A_TEST_FILE_DIRECTORY = "no_test_files";

    RelevanceCalculator calculator;
    
    @Before
    public void setUp() throws Exception {
        calculator = new RelevanceCalculator();
    }

    @Test
    public void testValidArgument() {
        final String fileDirectoryName = createDirectory(TEST_FILE_DIRECTORY);

        if (fileDirectoryName == null) {
            fail("Directory creation failure");
        }
        
        final String args[] = {fileDirectoryName};
               
        final boolean isValid = calculator.isValidFileDirectory(args);
        assertTrue("This is a valid command line argument", isValid);
        deleteDirectory(fileDirectoryName);
    }
    
    @Test
    public void testInvalidArgument_MoreThanOne() {
        String args[] = {TEST_FILE_DIRECTORY, NOT_A_TEST_FILE_DIRECTORY};

        final boolean isValid = calculator.isValidFileDirectory(args);
        assertFalse("Not allowed to have two command line arguments", isValid);

    }
    
    @Test
    public void testInvalidArgument_NotADirectory() {
        final String fileDirectoryName = getUnusedFileName(NOT_A_TEST_FILE_DIRECTORY);
        final String args[] = {fileDirectoryName};
        
        final boolean isValid = calculator.isValidFileDirectory(args);
        assertFalse("The directory " + fileDirectoryName + " does not exist.", isValid);
    }
    
    @Test
    public void testNoArgument() {
        String args[] = null;

        final boolean isValid = calculator.isValidFileDirectory(args);
        assertFalse("The arguments are null", isValid);
    }
    
    /*
     * Gets a unique filename to use as the directory name, then creates the directory.
     */
    private String createDirectory(final String fileNameBase) {
        
        String returnFileName = getUnusedFileName(fileNameBase);
        try {
            Files.createDirectories(Paths.get(returnFileName));
        } catch (IOException e) {
            return null;
        }
        
        return returnFileName;
        
    }
    
    private void deleteDirectory(final String fileName) {
        try {
            Files.deleteIfExists(Paths.get(fileName));
        } catch (IOException e) {            
        }
    }
    
    /*
     * Returns an unused filename that can be safely used to create a temporary directory
     * or test non-existent directory.
     */
    private String getUnusedFileName(final String fileNameBase) {
        String returnFileName = fileNameBase;
        int counter = 1;
        
        while (Files.isDirectory(Paths.get(returnFileName))) {
            returnFileName = fileNameBase + counter++;    
        }   
        
        return returnFileName;
    }

}
